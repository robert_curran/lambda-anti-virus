import pwd
import os
import subprocess
from common import AV_DEFINITION_PATH
from common import AV_DEFINITION_S3_BUCKET
from common import AV_DEFINITION_S3_PREFIX
from common import CLAMAVLIB_PATH
from common import FRESHCLAM_PATH
from common import create_dir
from common import file_md5
from clamav import defs_files
from s3 import md5_from_tags
from s3 import tag_object
from s3 import upload_file


def lambda_handler(event, context):
    """
    Lambda handler to get and update AV defs to S3

    Args:
        event (dict): AWS event triggering function to run
        context (dict): Context of the running Lambda functions

    Returns:
        None
    """
    update_defs_from_freshclam(AV_DEFINITION_PATH)

    upload_defs_to_s3(
        AV_DEFINITION_S3_BUCKET,
        AV_DEFINITION_S3_PREFIX,
        AV_DEFINITION_PATH
    )


def update_defs_from_freshclam(path):
    """
    Gets the lastest av defs for ClamAV using FreshClam

    Args:
        path(str): Path to save defs to

    Returns:
        int: Command return code
    """
    create_dir(path)

    freshclam_env = os.environ.copy()
    freshclam_env["LD_LIBRARY_PATH"] = CLAMAVLIB_PATH

    print("Starting freshclam with defs in {}.".format(path))

    freshclam_proc = subprocess.Popen(
        [
            FRESHCLAM_PATH,
            "--config-file=/opt/bin/freshclam.conf",
            "-u %s" % pwd.getpwuid(os.getuid())[0],
            "--datadir=%s" % path,
        ],
        stderr=subprocess.STDOUT,
        stdout=subprocess.PIPE,
        env=freshclam_env,
    )

    output = freshclam_proc.communicate()[0]

    print("freshclam output: {}".format(output))

    if freshclam_proc.returncode != 0:
        print("Unexpected exit code from freshclam: {}.".format(
            freshclam_proc.returncode))

    return freshclam_proc.returncode


def upload_defs_to_s3(bucket, prefix, local_path):
    """
    Uploads AV defs to S3 if they are changed from the current versions

    Args:
        bucket(str): S3 bucket name for AV def files
        prefix(str): S3 prefix for AV def files
        local_path(str): Path to AV def files locally

    Returns:
        None
    """
    for filename in defs_files():
        local_file_path = os.path.join(local_path, filename)
        s3_file_path = os.path.join(prefix, filename)

        if os.path.exists(local_file_path):
            local_file_md5 = file_md5(local_file_path)
            s3_md5 = md5_from_tags(bucket, s3_file_path)

            if local_file_md5 != s3_md5:
                upload_file(bucket, prefix, local_path, filename)
                tag_object(bucket, s3_file_path, "md5", local_file_md5)
            else:
                print("Not uploading {} because md5s match.".format(filename))
        else:
            print("File does not exist: {}.".format(local_file_path))
