import boto3
import botocore
import os
import subprocess
import urllib
from common import AV_DEFINITION_FILE_PREFIXES
from common import AV_DEFINITION_FILE_SUFFIXES
from common import AV_DEFINITION_PATH
from common import AV_DEFINITION_S3_BUCKET
from common import AV_DEFINITION_S3_PREFIX
from common import CLAMAVLIB_PATH
from common import CLAMSCAN_PATH
from common import get_timestamp
from common import create_dir


def lambda_handler(event, context):
    s3 = boto3.resource("s3")

    start_time = get_timestamp()
    print("Script starting at %s\n" % (start_time))

    s3_object = event_object(event)

    file_path = get_local_path(s3_object, "/tmp")
    create_dir(os.path.dirname(file_path))
    s3_object.download_file(file_path)

    update_defs_from_s3(s3, AV_DEFINITION_S3_BUCKET, AV_DEFINITION_S3_PREFIX)

    scan_result = scan_file(file_path)

    print(
        "Scan of s3://%s resulted in %s\n"
        % (os.path.join(s3_object.bucket_name, s3_object.key), scan_result)
    )

    # Delete downloaded file to free up room on re-usable lambda function container
    try:
        os.remove(file_path)
    except OSError:
        pass

    stop_scan_time = get_timestamp()
    print("Script finished at %s\n" % stop_scan_time)


def get_local_path(s3_object, local_prefix):
    return os.path.join(local_prefix, s3_object.bucket_name, s3_object.key)


def event_object(event):
    # Break down the record
    records = event["Records"]
    if len(records) == 0:
        raise Exception("No records found in event!")
    record = records[0]

    s3_obj = record["s3"]

    # Get the bucket name
    if "bucket" not in s3_obj:
        raise Exception("No bucket found in event!")
    bucket_name = s3_obj["bucket"].get("name", None)

    # Get the key name
    if "object" not in s3_obj:
        raise Exception("No key found in event!")
    key_name = s3_obj["object"].get("key", None)

    # if key_name:
    #     key_name = urllib.parse.unquote_plus(key_name.encode("utf8"))

    # Ensure both bucket and key exist
    if (not bucket_name) or (not key_name):
        raise Exception(
            "Unable to retrieve object from event.\n{}".format(event))

    # Create and return the object
    s3 = boto3.resource("s3")
    return s3.Object(bucket_name, key_name)


def scan_file(path):
    clanscan_env = os.environ.copy()

    clanscan_env["LD_LIBRARY_PATH"] = CLAMAVLIB_PATH

    print("Starting clamscan of %s." % path)

    clamscan_proc = subprocess.Popen(
        [CLAMSCAN_PATH, "-v", "-a", "--stdout", "-d", AV_DEFINITION_PATH, path],
        stderr=subprocess.STDOUT,
        stdout=subprocess.PIPE,
        env=clanscan_env,
    )

    output = clamscan_proc.communicate()[0]

    print("clamscan output:\n%s" % output)

    return output

    # Turn the output into a data source we can read
    # summary = scan_output_to_json(output)
    # if clamscan_proc.returncode == 0:
    #     return AV_STATUS_CLEAN, AV_SIGNATURE_OK
    # elif clamscan_proc.returncode == 1:
    #     signature = summary.get(path, AV_SIGNATURE_UNKNOWN)
    #     return AV_STATUS_INFECTED, signature
    # else:
    #     msg = "Unexpected exit code from clamscan: %s.\n" % clamscan_proc.returncode
    #     print(msg)
    #     raise Exception(msg)


def update_defs_from_s3(s3, bucket, prefix):
    create_dir(AV_DEFINITION_PATH)

    for file_prefix in AV_DEFINITION_FILE_PREFIXES:
        for file_suffix in AV_DEFINITION_FILE_SUFFIXES:

            filename = file_prefix + "." + file_suffix

            s3_path = os.path.join(prefix, filename)

            local_path = os.path.join(AV_DEFINITION_PATH, filename)

            print("Downloading definition file %s from s3://%s/%s" %
                  (local_path, bucket, s3_path))

            try:
                s3.Bucket(bucket).download_file(s3_path, local_path)
            except botocore.exceptions.ClientError as e:
                expected_errors = {"404", "AccessDenied", "NoSuchKey"}
                if e.response["Error"]["Code"] not in expected_errors:
                    raise

            print("Downloading definition file %s complete!" % (local_path))
