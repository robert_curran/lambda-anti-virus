import os
import subprocess

from common import CLAMAVLIB_PATH
from common import CLAMSCAN_PATH


def lambda_handler(event, context):
    av_env = os.environ.copy()

    av_env["LD_LIBRARY_PATH"] = CLAMAVLIB_PATH

    av_proc = subprocess.Popen(
        [CLAMSCAN_PATH, "-version"],
        stderr=subprocess.STDOUT,
        stdout=subprocess.PIPE,
        env=av_env,
    )

    output = av_proc.communicate()[0]

    print("clamscan output:\n%s" % output)
