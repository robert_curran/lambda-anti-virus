current_dir := $(shell pwd)

all: build

`.PHONY: clean
clean:
	rm -rf clamav-layer/

.PHONY: build
build: clean
	docker build -t clamav-layer:latest .
	mkdir -p ./clamav-layer/
	docker run -v $(current_dir)/clamav-layer:/opt/mount --rm --entrypoint cp clamav-layer:latest -R /opt/bin/ /opt/mount/

# clamav.update_defs_from_freshclam(AV_DEFINITION_PATH, CLAMAVLIB_PATH)

# /tmp/clamav_defs ./bin
